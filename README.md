Perswin
=======
A personal window to show personal works.
It will be create to make Manudev.be website.

Licence
=======
Creative commons 
Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 2.0 Belgique

Install
=======
This project use the Django Python framework. Install it like a django project.
Don't forget to change Perswin/settings.dist.py for Perswin.settings.py with your configuration.


Admin
=====
You can access on the admin page with domain.tld/admin url.

Compressor
==========
The project use compressor django app : http://django_compressor.readthedocs.org/en/latest/quickstart/#installation