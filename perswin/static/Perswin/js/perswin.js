$(document).ready(function(){
	$(document).foundation();
	
	$('#bandShow').click(function(){
		$('#tiles').fadeOut('slow', function(){
			$('#bands').fadeIn('slow');
		});
		return false;
	});
	
	$('#tileShow').click(function(){
		$('#bands').fadeOut('slow', function(){
			$('#tiles').fadeIn('slow');
		});
		return false;
	});


	$('#sortProjectByName').click(function(){
		orderProjects('name');
		return false;
	});

	$('#sortProjectBySuposedDate').click(function(){
		orderProjects('supposed_date');
		return false;
	});
	
	// sortProjectByDate
	$('#TagfilterInput').focusin(function(){
		$('#TagList').fadeIn();
	});
	$('#TagfilterInput').focusout(function(){
		$('#TagList').fadeOut();
	});
	
	$('#TagfilterInput').keyup(function(){
		filterTagProjects($(this).val(), $(this).attr('data-order'));
	});
});

function filterTagProjects(tags, order){

	var currentShow = $('#bands').is(':visible') ? '#bands' : '#tiles'; 
	var currentHide = !$('#bands').is(':visible') ? '#bands' : '#tiles'; 
	var link = "";
	if (typeof order == 'undefined'){
		link = 'projects/tag/'+tags;
	} else {
		link = 'projects/'+order+'/'+tags;
	}

	$.ajax({
		url: link,
		method: 'GET',
		success : function(data){
			$('#projects').html(data);
			$(currentShow).show();
			$(currentHide).hide();
		}
	});
}

function orderProjects(order){
	$('#TagfilterInput').attr('data-order', order);
	$.ajax({
		url: 'projects/'+order,
		method: 'GET',
		success : function(data){
			$('#projects').html(data);
		}
	});
};