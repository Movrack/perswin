 bkLib.onDomLoaded(function() {
        nicEditors.allTextAreas({iconsPath : '/static/nicEdit/nicEditorIcons.gif',
            buttonList : ['fontSize','fontFamily','bold','italic',
                'underline','strikeThrough','left','center','right','justify',
                'ol','ul','subscript','superscript','hr','link','unlink','forecolor',
                'image','upload','xhtml']
        });
    });