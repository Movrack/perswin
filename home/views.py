# from django.template import Context, loader
from django.shortcuts import render_to_response
from django.http import HttpResponse

def index(request):
    # return HttpResponse("Hello, world. You're at the poll index.")

    # t = loader.get_template('home/index.html')
    # c = Context({ })
    # return HttpResponse(t.render(c))

    return render_to_response('home/index.html', {})


def licence(request):
    return render_to_response('home/licence.html', {})


def contact(request):
    return render_to_response('home/contact.html', {})