from django.conf.urls import patterns, url

urlpatterns = patterns('home.views',
    url(r'^$', 'index'),
    url(r'^licence$', 'licence'),
    url(r'^contact$', 'contact'),
    # url(r'^evaluate/$', 'evaluate'),
)