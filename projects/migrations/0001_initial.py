# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Picture'
        db.create_table(u'projects_picture', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
        ))
        db.send_create_signal(u'projects', ['Picture'])

        # Adding model 'WebSite'
        db.create_table(u'projects_website', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('website', self.gf('django.db.models.fields.URLField')(max_length=200)),
        ))
        db.send_create_signal(u'projects', ['WebSite'])

        # Adding model 'Partner'
        db.create_table(u'projects_partner', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('firstname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('lastname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('website', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['projects.WebSite'], null=True, blank=True)),
        ))
        db.send_create_signal(u'projects', ['Partner'])

        # Adding model 'Tag'
        db.create_table(u'projects_tag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'projects', ['Tag'])

        # Adding model 'Project'
        db.create_table(u'projects_project', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('abstract', self.gf('django.db.models.fields.TextField')()),
            ('body', self.gf('django.db.models.fields.TextField')()),
            ('date', self.gf('django.db.models.fields.DateField')(auto_now=True, blank=True)),
            ('supposed_date', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2013, 3, 5, 0, 0))),
            ('first_picture', self.gf('django.db.models.fields.related.ForeignKey')(default=None, related_name='first_picture_set', null=True, blank=True, to=orm['projects.Picture'])),
        ))
        db.send_create_signal(u'projects', ['Project'])

        # Adding M2M table for field pictures on 'Project'
        db.create_table(u'projects_project_pictures', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'projects.project'], null=False)),
            ('picture', models.ForeignKey(orm[u'projects.picture'], null=False))
        ))
        db.create_unique(u'projects_project_pictures', ['project_id', 'picture_id'])

        # Adding M2M table for field partners on 'Project'
        db.create_table(u'projects_project_partners', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'projects.project'], null=False)),
            ('partner', models.ForeignKey(orm[u'projects.partner'], null=False))
        ))
        db.create_unique(u'projects_project_partners', ['project_id', 'partner_id'])

        # Adding M2M table for field tags on 'Project'
        db.create_table(u'projects_project_tags', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('project', models.ForeignKey(orm[u'projects.project'], null=False)),
            ('tag', models.ForeignKey(orm[u'projects.tag'], null=False))
        ))
        db.create_unique(u'projects_project_tags', ['project_id', 'tag_id'])


    def backwards(self, orm):
        # Deleting model 'Picture'
        db.delete_table(u'projects_picture')

        # Deleting model 'WebSite'
        db.delete_table(u'projects_website')

        # Deleting model 'Partner'
        db.delete_table(u'projects_partner')

        # Deleting model 'Tag'
        db.delete_table(u'projects_tag')

        # Deleting model 'Project'
        db.delete_table(u'projects_project')

        # Removing M2M table for field pictures on 'Project'
        db.delete_table('projects_project_pictures')

        # Removing M2M table for field partners on 'Project'
        db.delete_table('projects_project_partners')

        # Removing M2M table for field tags on 'Project'
        db.delete_table('projects_project_tags')


    models = {
        u'projects.partner': {
            'Meta': {'object_name': 'Partner'},
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'firstname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'website': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['projects.WebSite']", 'null': 'True', 'blank': 'True'})
        },
        u'projects.picture': {
            'Meta': {'object_name': 'Picture'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        u'projects.project': {
            'Meta': {'object_name': 'Project'},
            'abstract': ('django.db.models.fields.TextField', [], {}),
            'body': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'blank': 'True'}),
            'first_picture': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'related_name': "'first_picture_set'", 'null': 'True', 'blank': 'True', 'to': u"orm['projects.Picture']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'partners': ('django.db.models.fields.related.ManyToManyField', [], {'default': 'None', 'to': u"orm['projects.Partner']", 'null': 'True', 'symmetrical': 'False', 'blank': 'True'}),
            'pictures': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'pictures_set'", 'default': 'None', 'to': u"orm['projects.Picture']", 'blank': 'True', 'symmetrical': 'False', 'null': 'True'}),
            'supposed_date': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(2013, 3, 5, 0, 0)'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'default': 'None', 'to': u"orm['projects.Tag']", 'null': 'True', 'symmetrical': 'False', 'blank': 'True'})
        },
        u'projects.tag': {
            'Meta': {'object_name': 'Tag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'projects.website': {
            'Meta': {'object_name': 'WebSite'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'website': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['projects']