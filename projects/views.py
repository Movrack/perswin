# Create your views here.
from django.shortcuts import render_to_response
from django.http import HttpResponse
from projects.models import Project, Tag
import logging
logger = logging.getLogger(__name__)

def index(request):
    project_list = Project.objects.all().order_by('name')
    tag_list = Tag.objects.all().order_by('label')
    return render_to_response('projects/index.html', 
        {
            "project_list": project_list,
            "tag_list": tag_list
        }
    )

def order_by(request, order_by="name"):
    if(order_by == "supposed_date") :
        project_list = Project.objects.all().order_by('supposed_date')
    elif(order_by == "date") :
        project_list = Project.objects.all().order_by('date')
    else :
        project_list = Project.objects.all().order_by('name')

    return render_to_response('projects/projectList.html', 
        {"project_list": project_list}
    )

def tags(request, tags="", order_by="name"):
    tag_list = tags.split(" ")
    project_list = Project.objects.all().order_by(order_by)
    for tag in tag_list :
        project_list = project_list.filter(tags__label__icontains=tag).distinct()

    return render_to_response('projects/projectList.html', 
        {"project_list": project_list}
    )