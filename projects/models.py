from django.db import models
from datetime import datetime

# Create your models here.
class Picture(models.Model):
    picture = models.ImageField(upload_to="project/pictures")
    def __unicode__(self):
        return self.picture

class WebSite(models.Model):
    name = models.CharField(max_length=100)
    website = models.URLField()
    def __unicode__(self):
        return self.name

class Partner(models.Model):
    firstname = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    email = models.EmailField()
    website = models.ForeignKey(WebSite, default=None, null=True, blank=True)
    def __unicode__(self):
        return self.firstname + self.lastname

class Tag(models.Model):
    label = models.CharField(max_length=100)
    def __unicode__(self):
        return self.label

class Project(models.Model):
    name            = models.CharField(max_length=200)
    abstract        = models.TextField()
    body            = models.TextField()
    date            = models.DateField(auto_now=True)
    supposed_date   = models.DateField(default=datetime.today)
    first_picture   = models.ForeignKey(Picture, null=True, blank=True, default=None, related_name="first_picture_set")
    pictures        = models.ManyToManyField(Picture, null=True, blank=True, default=None, related_name="pictures_set")
    partners        = models.ManyToManyField(Partner, null=True, blank=True, default=None)
    tags            = models.ManyToManyField(Tag, null=True, blank=True, default=None)
    def __unicode__(self):
        return self.name