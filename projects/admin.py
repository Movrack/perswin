from projects.models import Tag, WebSite, Partner, Picture, Project
from django.contrib import admin
# from projects.forms import ProjectAdminModelForm 

admin.site.register(WebSite)
admin.site.register(Partner)
admin.site.register(Picture)
admin.site.register(Tag)
# admin.site.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    class Media:
        js = ("/static/nicEdit/nicEdit.js",
        	"/static/nicEdit/admin.js")
	 
admin.site.register(Project, ProjectAdmin)
