from django.conf.urls import patterns, url

urlpatterns = patterns('projects.views',
    url(r'^$', 'index'),
    url(r'^/(?P<order_by>\w{0,50})/$', 'order_by'),
    url(r'^/tag/(?P<tags>[(\w ]{0,50})/$', 'tags'),
    url(r'^/(?P<order_by>\w{0,50})/(?P<tags>[\w ]{0,50})/$', 'tags'),
)